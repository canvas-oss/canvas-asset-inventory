#CANVAS 

CANVAS Common Application & Network Vulnerability Assessment System is a computer program whose purpose is to to identify and assess known vulnerabilities on an information system.

##CANVAS ASSET INVENTORY

CANVAS ASSET INVENTORY is the component responsible for gathering the applications, hardware and operating systems informations of a system and send them to the main database for correlation.
For in-depth documentation, please browse to the components' directories (agent & server).

##Licence

CANVAS including all its components is distributed under the CeCILL licence. Please see the LICENSE.CECILL-FR and LICENCE-CECILL-EN documents for more informations.

##Documentation

The CANVAS general documentation is available at the main repository's wiki. A specific documentation is available on this repository's wiki.

##Contribution guidelines

You can contribute to this project. Please contact us if you have any question. All pull requests will be reviewed by us.

##Contact

You can contact us in the email provided in the licence.

###Issues 

You can open issues at the [CANVAS's JIRA Dashboard](https://canvas-oss.atlassian.net/projects/CANVAS/issues). You have to be logged in to open an issue.