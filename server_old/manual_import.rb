# ----------------------------------------------------------------------------
# Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
# 2017-10-30T17:25:08+01:00
#
# boissezon.remy@gmail.com
# valentin@prodhomme.me
# chill3d@protonmail.com
# alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ----------------------------------------------------------------------------
require 'mysql2'
require 'yaml'
require 'ox'
require './wfn.rb'

# ----------------------------------------------------------------------------
#                             Parsing functions
# ----------------------------------------------------------------------------

# XML parser and insert function
# Main function
def parse_import_xml
  # Connect Database
  db = database_connection
  doc = open_xml
  host_list = doc.locate('root/host')
  # print host_list
  host_list.each do |host|
    host_name = db.escape(host.locate('hostname')[0].text)
    # host insertion
    continue if insert_host(db, host_name) == False
    # parcours assets
    asset_list = host.locate('assets/asset')
    asset_list.each do |asset|
      asset_wfn = asset.locate('wfn')[0].text
      find_wfn_id(db, asset_wfn)
    end
  end
end

# Gets filepath for file to import
def import_filepath
  accepted_extensions = ['.xml']
  puts 'Please enter the filepath of your .xml file'
  filepath = gets.chomp
  raise ArgumentError, 'No filepath found' if filepath.empty?
  raise ArgumentError, 'Error : wrong file type' unless accepted_extensions.include? File.extname(filepath)
  return filepath
end

# XML Open function
def open_xml
  xml_doc = Ox.parse(File.open(import_filepath, 'r:UTF-8', &:read))
  return xml_doc
end

# ----------------------------------------------------------------------------
#                             Database functions
# ----------------------------------------------------------------------------

# Get database connection configuration
def database_conf
  conf = YAML.load_file('./conf.yml')
  [conf['database']['adress'], conf['database']['user'], conf['database']['password']]
end

# Connexion to database
def database_connection
  dbaddr, dbuser, dbpasswd = database_conf
  db_con = Mysql.new dbaddr, dbuser, dbpasswd
  return dbCon
rescue Mysql::Error => e
  log(e.errno)
  log(e.error)
  db_con.close if dbCon
  return nil
end

# ----------------------------------------------------------------------------
#                      Database manipulation functions
# ----------------------------------------------------------------------------

# Insert function
def find_wfn_id(db, asset_wfn)
  # Prepare query arguments
  tmp = asset_wfn.split(':')
  wfn = WfnElement.new(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4])
  # Prepare select wfn id query
  return wfn.select_wfn_id_query(db)
end

# Insert host query
# returns True if host either exists or is inserted
# returns False if host doesn't exists and insertion failed
def insert_host(db, host_name)
  # Prepare insert query
  insert = "INSERT INTO t_host_item (host_name) VALUES '#{host_name}'"
  if select_host(db, host_name)
    log('Existing Host : Host not inserted')
    return True
  elsif db.query(insert)
    log('host inserted')
    return True
  else
    log('Host insertion failed')
    return False
  end
end

# Select Host query
def select_host(db, host_name)
  return db.query("SELECT id FROM t_host_item WHERE host_name='#{host_name}'")
end

# ----------------------------------------------------------------------------
#                              Log functions
# ----------------------------------------------------------------------------

# log function
def log(msg)
  puts msg
end
