# ----------------------------------------------------------------------------
# Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
# 2017-11-20T16:52:45+01:00
#
# boissezon.remy@gmail.com
# valentin@prodhomme.me
# chill3d@protonmail.com
# alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ----------------------------------------------------------------------------

# Class for WFN elements
class WfnElement
  attr_accessor :part, :vendor, :product, :version, :update
  # Initialization
  def initialize(wfn)
    @part = wfn[0]
    @vendor = wfn[1]
    @product = wfn[2]
    @version = wfn[3]
    @update = wfn[4]
  end

  def select_wfn_id_query(db)
    # Initialization
    vendor = ''
    product = ''
    version = ''
    update = ''
    # Conditionnal Instantiation
    vendor = "AND vendor='#{@vendor}' " if @vendor != '*'
    product = "AND product='#{@product}' " if @product != '*'
    version = "AND version='#{@version}' " if @version != '*'
    update = "AND update='#{@update}'" if @update != '*'
    # Forge request
    requete = "SELECT id FROM t_wfn_item WHERE part='#{@part}' ".concat(vendor).concat(product).concat(version).concat(update)
    # Query database
    return db.query(requete)
  end
end
