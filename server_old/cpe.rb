# ----------------------------------------------------------------------------
# Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
# 2017-10-30T17:25:08+01:00
#
# boissezon.remy@gmail.com
# valentin@prodhomme.me
# chill3d@protonmail.com
# alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ----------------------------------------------------------------------------
class CpeElement
  attr_accessor :part, :vendor, :product, :version, :edition, :update, :language, :sw_edition, :target_sw, :target_hw, :other
  # Initialization
  def initialize(part = nil, vendor = nil, product = nil, version = nil, edition = nil, update = nil, language = nil, sw_edition = nil, target_sw = nil, target_hw = nil, other = nil)
    @part = part
    @vendor = vendor
    @product = product
    @version = version
    @edition = edition
    @update = update
    @language = language
    @sw_edition = sw_edition
    @target_sw = target_sw
    @target_hw = target_hw
    @other = other
  end
end
