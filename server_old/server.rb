# ----------------------------------------------------------------------------
# Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
# 2017-10-30T17:47:00+01:00
#
# boissezon.remy@gmail.com
# valentin@prodhomme.me
# chill3d@protonmail.com
# alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ----------------------------------------------------------------------------

require 'socket'

server = TCPServer.open(5555)
eof = 0

loop do
  print 'server started'
  Thread.start(server.accept) do |agent|
    print 'TCP CON STARTED'
    line = agent.gets
    if line != "Client_Hello\n"
      print 'wrong server'
      agent.close
    else
      agent.puts('Server_Hello')
      while eof != 0
        line = agent.recv(4096)
        if line == 'eof'
          eof = 1
        else
          xml += line
        end
      end
      res = parse_xml(xml)
      if res == 1
        agent.puts('RECV_OK')
      else
        agent.puts('RECV_NOK')
      end
    end
  end
end
