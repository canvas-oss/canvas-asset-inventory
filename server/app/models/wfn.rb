# ----------------------------------------------------------------------------
# Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
# 2017-11-20T16:52:45+01:00
#
# boissezon.remy@gmail.com
# valentin@prodhomme.me
# chill3d@protonmail.com
# alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ----------------------------------------------------------------------------

# Class for WFN elements
class WfnElement
  attr_accessor :part, :vendor, :product, :version, :update, :desc
  # Initialization
  def initialize(wfn)
    @part = wfn[0]
    @vendor = wfn[1]
    @product = wfn[2]
    @version = wfn[3]
    @update = wfn[4]
    @desc = wfn[5]
  end

  def self.select_wfn_id_query(db, host_id, generator_id)
    # Forge request
    requete = db.prepare('call p_asset_save_asset(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                          ?, @asset_id);')
    retrieve_asset_id = db.preprare('SELECT @asset_id AS asset_id;')
    # Query database
    sql_transaction(db) do
      requete.execute(host_id, @part, @vendor, @product, @version, @update,
                      '*', '*', '*', '*', generator_id)
      return retrieve_asset_id.execute.first['asset_id']
    end
  end

  def sql_transaction(sql_client)
    sql_client.query('BEGIN;')
    yield
    sql_client.query('COMMIT;')
  rescue Mysql2::Error => e
    puts("SQL Transaction error: #{e}")
    sql_client.query('ROLLBACK;')
  end
end
