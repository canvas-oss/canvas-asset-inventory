# ----------------------------------------------------------------------------
# Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
# 2018-01-23T15:36:05+01:00
#
# boissezon.remy@gmail.com
# valentin@prodhomme.me
# chill3d@protonmail.com
# alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ----------------------------------------------------------------------------
require 'yaml'

# Assets class model to parse and save to DB assets
class Assets
  attr_accessor :dbaddr, :dbuser, :dbpasswd
  def initialize
    @dbaddr, @dbuser, @dbpasswd = database_conf
    @db = database_connection
  end

  def self.upload(upload_file)
    # open_xml(upload_file)
    doc = File.open(upload_file.path, 'r:UTF-8', &:read)
    doc.each_line do |line|
      puts line
    end
  end

  # ----------------------------------------------------------------------------
  #                             Parsing functions
  # ----------------------------------------------------------------------------

  def open_xml(upload_file)
    xml_doc = Ox.parse(File.open(upload_file.path, 'r:UTF-8', &:read))
    return xml_doc
  end

  # XML parser and insert function
  # Main function
  def parse_import_xml
    generator_id = ''
    # Connect Database
    doc = open_xml
    # print host_list
    host_name = @db.escape(doc.locate('HOSTNAME')[0].text)
    # host insertion
    gen_req1 = @db.prepare('call p_asset_start_update_process("server_canvas", "1", "agent_canvas", "1", @generator_id);')
    gen_select = @db.prepare('SELECT @generator_id AS generator_id;')
    sql_transaction(@db) do
      gen_req1.execute
      generator_id = gen_select.execute.first['generator_id']
    end

    host_id = check_host(@db, host_name, generator_id)
    # parcours assets
    asset_list = doc.locate('APPLIST/APP')
    asset_list.each do |asset|
      app_part    = asset.locate('APP_PART')[0].text
      app_vendor  = asset.locate('APP_VEND')[0].text
      app_name    = asset.locate('APP_PROD')[0].text
      app_vers    = asset.locate('APP_VERS')[0].text.split(':')[O].split('-')[0]
      app_updt    = asset.locate('APP_UPDT')[0].text
      app_desc    = asset.locate('APP_DESC')[0].text
      wfn = WfnElement.new(app_part, app_vendor, app_name, app_vers, app_updt, app_desc)
      asset_id = wfn.select_wfn_id_query(@db, host_id, generator_id)
    end
  end

  # ----------------------------------------------------------------------------
  #                      Database manipulation functions
  # ----------------------------------------------------------------------------

  # Insert host query
  # returns True if host either exists or is inserted
  # returns False if host doesn't exists and insertion failed
  def check_host(db, host_name, generator_id)
    req = db.prepare('call p_asset_save_host(INET_ATON("192.168.1.200"), INET_ATON("255.255.255.0"), ?, ? @host_id);')
    host_id_req = db.prepare('SELECT @host_id AS host_id')
    sql_transaction do
      req.execute(host_name, generator_id)
      return host_id_req.execute.first['host_id']
    end
  end

  # insert host_name into DB
  def insert_host(host_name)
    return @db.query("INSERT INTO t_host_item (host_name) VALUES '#{host_name}'")
  end

  # Select Host query
  def select_host(host_name)
    return @db.query('call p_asset_start_update_process("server_canvas", "1", "agent_canvas", "1", @generator_id);')
  end

  # ----------------------------------------------------------------------------
  #                             Database functions
  # ----------------------------------------------------------------------------

  def self.database_conf
    conf = YAML.load_file('../../config/conf.yml')
    [conf['database']['adress'], conf['database']['user'], conf['database']['password']]
  end

  def self.database_connection
    db_con = Mysql.new @dbaddr, @dbuser, @dbpasswd
    return dbCon
  rescue Mysql::Error => e
    log(e.errno)
    log(e.error)
    db_con.close if dbCon
    return nil
  end

  def sql_transaction(sql_client)
    sql_client.query('BEGIN;')
    yield
    sql_client.query('COMMIT;')
  rescue Mysql2::Error => e
    puts("SQL Transaction error: #{e}")
    sql_client.query('ROLLBACK;')
  end
end
