# README

This server is using ruby on rails and uses the database from CANVAS CORE.

* Ruby version
	* ruby 2.4


* Rails version
	* rails 5.1.4


* System dependencies
	* Please see the Gemfile for gem dependencies


* Configuration
	* Your databse configuration must be in the config/conf.yml file


* How to run the test suite
	* ``` rails s ```

