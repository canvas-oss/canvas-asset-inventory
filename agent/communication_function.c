/* ----------------------------------------------------------------------------
* Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
* 2017-10-30T17:24:59+01:00
*
* boissezon.remy@gmail.com
* valentin@prodhomme.me
* chill3d@protonmail.com
* alexandre.zanni@engineer.com
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software.  You can  use,
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
---------------------------------------------------------------------------- */
#include "./communication_function.h"

/* Socket creation function ; returns the socket */
int create_socket(){
  SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
  SOCKADDR_IN sin = { 0 };
  if(sock == INVALID_SOCKET)
  {
    perror("socket()");
    exit(errno);
  }
   sin.sin_addr = *(IN_ADDR *) IP_ADDR;
   sin.sin_port = htons(IP_PORT);
   sin.sin_family = AF_INET;
   if(connect(sock,(SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
   {
      perror("connect()");
      exit(errno);
   }
   return sock;
}

/* closes the socket */
static void close_socket(socket){
  closesocket(socket);
}

static int read_server(SOCKET sock, char *buffer){
  int n = 0;
  if((n = recv(sock, buffer, BUFFER_SIZE - 1, 0))<0){
    perror("recv()");
    ecit(errno);
  }
  buffer[n] = 0;
  return n;
}

static void send_server(SOCKET socket, const char* msg){
  if(send(socket, msg, strlen(msg), 0)<0){
    perror("send()");
    exit(errno);
  }
}

int send_data(char* filepath){
  SOCKET socket = create_socket();
  char buffer[BUFFER_SIZE];
  fd_set rdfs;
  int connection_state = 1;
  int n;
  int retval;
  /* Set Timeout */
  struct timeval tv;
  tv.tv_sec = 5;
  tv.tv_usec = 0;

  send_server_HELLO(socket, SERVER_HELLO);
  while(connection_state == 1){
    FD_ZERO(&rdfs);
    FD_SET(sock, &rdfs);
    retval = select(sock+1, &rdfs, NULL, NULL, &tv);
    if( retval == -1){
      perror("select()");
      exit(errno);
    }else if (retval){
      if(FD_ISSET(sock, &rdfs)){
        n = read_server(sock, buffer)
        if(n==0){
          printf("server disconnected");
          connection_state = 0;
          break;
        }
        if(buffer =! CLIENT_HELLO){
          printf("Wrong server");
          connection_state = 0;
          break;
        }
      }else{
        printf("server Timed Out");
        connection_state = 0;
        break;
      }
    }
  }
}

int send_xml(char* filepath){

  return EXIT_SUCCESS;
}
