/* ----------------------------------------------------------------------------
* Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
* 2017-10-30T17:24:59+01:00
*
* boissezon.remy@gmail.com
* valentin@prodhomme.me
* chill3d@protonmail.com
* alexandre.zanni@engineer.com
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software.  You can  use,
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
---------------------------------------------------------------------------- */

#include <string.h>
#include <stdio.h>

#define EXIT_SUCCESS    0
#define EXIT_FAILURE    1

#ifdef __linux__
#include "./linux_asset_gathering_functions.h"
#include "./httpput.h"
#define filepath "/tmp/.applist"
#define xml_filepath "/tmp/applist.xml"
#define url "http://localhost:3000/upload_file"
#define LIST_APPS(){list_debian_packages(filepath);}
#define SEND_CURL(){curl_send_file(xml_filepath, url);}
#endif

int main (int argc, char *argv[]){
  LIST_APPS();
  SEND_CURL();
	return 0;
}
