#Agent

This is the documentation file for the CANVAS's Agent.

##Supported OS

This program is supported under any UNIX system's using DPKG :
- Tested with Debian

##Dependencies

Curl Open SLL Library - This program must be compiled with the curl library.

##Steps 

- Close this repository
- Compile the agent and launch it
``` 
# make
# ./agent 
```
