/* ----------------------------------------------------------------------------
* Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
* 2017-10-30T17:24:59+01:00
*
* boissezon.remy@gmail.com
* valentin@prodhomme.me
* chill3d@protonmail.com
* alexandre.zanni@engineer.com
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software.  You can  use,
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
---------------------------------------------------------------------------- */

#ifndef COMMUNICATION_FUNCTION_H
#define COMMUNICATION_FUNCTION_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/time.h>

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define BUFFER_SIZE 4096

#define SERVER_HELLO "Server_Hello"
#define CLIENT_HELLO "Client_Hello"

#define IP_ADDR "127.0.0.1"
#define IP_PORT 5555

struct sockaddr_in {
   uint8_t         sin_len;       /* longueur totale      */
   sa_family_t     sin_family;    /* famille : AF_INET     */
   in_port_t       sin_port;      /* le numéro de port    */
   struct in_addr  sin_addr;      /* l'adresse internet   */
   unsigned char   sin_zero[8];   /* un champ de 8 zéros  */
};

int create_socket();
static void close_socket(socket);
static int read_server(SOCKET sock, char *buffer);
static void send_server_HELLO(SOCKET socket, const char* msg);
int send_xml(char* filepath);

#endif
