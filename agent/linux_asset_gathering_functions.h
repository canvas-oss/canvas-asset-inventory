/* ----------------------------------------------------------------------------
* Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
* 2017-11-20T16:56:06+01:00
*
* boissezon.remy@gmail.com
* valentin@prodhomme.me
* chill3d@protonmail.com
* alexandre.zanni@engineer.com
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software.  You can  use,
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
---------------------------------------------------------------------------- */

#ifndef ASSET_GATHERING_H
#define ASSET_GATHERING_H

#define SIZE_LOW_BUF 255
#define SIZE_HIGH_BUFFER 4096
#define ZERO 0

#define DKPG_CMD "dpkg -l > "
#define DPKG_FORMAT "ii %253[^ ] %253[^ ] %253[^ ] %253[^\n]"
#define XML_START "<ASSETS>\n\t<HOSTNAME>%s</HOSTNAME>\n\t<APPLIST>"
#define XML_END "\n\t</APPLIST>\n</ASSETS>"
#define XML_APP_PART "\n\t\t\t<APP_PART>a</APP_PART>"
#define XML_APP_VEND "\n\t\t\t<APP_VEND>*</APP_VEND>"
#define XML_APP_PROD "\n\t\t\t<APP_PROD>%s</APP_PROD>"
#define XML_APP_VERS "\n\t\t\t<APP_VERS>%s</APP_VERS>"
#define XML_APP_UPDT "\n\t\t\t<APP_UPDT>*</APP_UPDT>"
#define XML_APP_ARCH "\n\t\t\t<APP_ARCH>%s</APP_ARCH>"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/unistd.h>

struct application_values{
  char application_name[SIZE_LOW_BUF];
  char version[SIZE_LOW_BUF];
  char description[SIZE_LOW_BUF];
  char architecture[SIZE_LOW_BUF];
  char publisher[SIZE_LOW_BUF];
};

int list_debian_packages();
int read_applist_file(char* filename);
int parse_applist_line(char* line, FILE *xml);
int write_xml_debian(struct application_values apps, FILE *xml);
int prepare_xml_file(FILE *xml);
int end_xml_file(FILE *xml);

#endif
