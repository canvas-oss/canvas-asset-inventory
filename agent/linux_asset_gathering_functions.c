/* ----------------------------------------------------------------------------
* Copyright Remy BOISSEZON, Valentin PRODHOMME, Dylan TROLES, Alexandre ZANNI
* 2017-11-20T16:55:50+01:00
*
* boissezon.remy@gmail.com
* valentin@prodhomme.me
* chill3d@protonmail.com
* alexandre.zanni@engineer.com
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software.  You can  use,
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
---------------------------------------------------------------------------- */

#include "./linux_asset_gathering_functions.h"

/* print the list of applications intalled in a file at filepath*/
int list_debian_packages(char* filepath){
  int var = 1;
  FILE *f;
  char* arg = calloc(strlen(DKPG_CMD)+strlen(filepath)+1, sizeof(char));
  sprintf(arg, "%s%s", DKPG_CMD, filepath);
  if(!(f = popen(arg, "r"))){var = 0;}
  if(var == 1){
    read_applist_file(filepath);
  }
  pclose(f);
  free(arg);
  return EXIT_SUCCESS;
}

/* fonction de traitement ligne par ligne pour dpkg */
int read_applist_file(char* filepath){
  FILE *f;
  FILE *xml;
  size_t len = 0;
  ssize_t read;
  char* line = NULL;
  f = fopen(filepath, "r");
  xml = fopen("/tmp/applist.xml", "a");
  if(f == NULL){
    printf("applist file could not be created");
    fclose(f);
    fclose(xml);
    return(EXIT_FAILURE);
  }
  if(xml == NULL){
    printf("xml file could not be created");
    fclose(f);
    fclose(xml);
    return(EXIT_FAILURE);
  }
  prepare_xml_file(xml);
  while((read = getline(&line, &len, f)) != -1){
    parse_applist_line(line, xml);
    free(line);
    line = NULL;
  }
  end_xml_file(xml);
  fclose(f);
  fclose(xml);
  return EXIT_SUCCESS;
}

/* parser pour ligne ouput de dpkg */
int parse_applist_line(char* line, FILE *xml){
  struct application_values apps;
  memset(&apps, ZERO, sizeof(struct application_values));
  if(strncmp("ii", line, 2) != 0){return EXIT_SUCCESS;}
  sscanf(line, DPKG_FORMAT ,apps.application_name, apps.version, apps.architecture, apps.description);
  write_xml_debian(apps, xml);
  return EXIT_SUCCESS;
}

int write_xml_debian(struct application_values apps, FILE *xml){
  fprintf(xml, "\n\t\t<APP>");
  fprintf(xml, XML_APP_PART);
  fprintf(xml, XML_APP_VEND);
  fprintf(xml, XML_APP_PROD, apps.application_name);
  fprintf(xml, XML_APP_VERS, apps.version);
  fprintf(xml, XML_APP_UPDT);
  fprintf(xml, XML_APP_ARCH, apps.architecture);
  fprintf(xml, XML_APP_DESC, apps.description);
  fprintf(xml, "\n\t\t</APP>");
  return EXIT_SUCCESS;
}

int prepare_xml_file(FILE *xml){
  char hostname[SIZE_LOW_BUF];
  gethostname(hostname, sizeof(hostname));
  fprintf(xml, XML_START, hostname);
  return EXIT_SUCCESS;
}

int end_xml_file(FILE *xml){
  fprintf(xml, XML_END);
  return EXIT_SUCCESS;
}
